### Cluster-aware middleware management, Kombinator

Kombinator represents middleware in the form of HashiCorp configuration files for their products, such as Terraform, Nomad, etc...

LMPK franchises Kombinator's assets to independent platform promoters (franchisees), such as [Kombinowac.pl](https://kombinowac.pl).

LMPK provisions 3 levels of middleware for 3 scales of deployment that are elaborated upon in less technical language [here](https://framavox.org/d/Y7tUBxLi/what-is-the-vision-of-lmpk-):
1. Hosting-as-a-service for Platform Promoters
    LMPK's largest model provisions a cluster able to orchestrate a large number of servers on demand. In this model, each franchisee, like Kombinowac.pl, will own its own cluster.
    1. Terraform:
    2. Nomad:
2. Remote-host management
    Orgnizations large enough to host their own instances can leverage LMPK assets for their midsized needs.
    1. Terraform:
    2. Nomad
3. Self-hosting technical support
    Larger independent organizations can subscribe to LMPK's library here at GitLab for free access to constantly updated assets.
    1. Terraform:
    2. Nomad